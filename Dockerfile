FROM tensorflow/tensorflow:1.15.2-gpu-py3-jupyter

RUN apt-get update && apt-get install -y libncurses-dev wget

RUN adduser --uid 1000 tf
RUN mkdir /.cache 
RUN chown -R tf:tf /.cache

RUN pip install --upgrade pip
RUN pip install matplotlib seaborn Keras pandas scipy h5py future Pillow scikit-image scikit-learn torchvision tqdm sentencepiece segtok 

# jupyter-datakit

This docker image is based on the tensorflow:2.1.0-gpu-py3-jupyter

Example command:

```
docker image build . -t jupyter-datakit
docker run --gpus all -p 8888:8888 --name datakit -u $(id -u ${USER}):$(id -g ${USER}) -v $(pwd):/tf jupyter-datakit
```

